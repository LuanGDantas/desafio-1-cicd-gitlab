# Desafio de Projeto Pipeline de Criação e Execução de uma Imagem Docker

## DESCRIÇÃO

Criar um pipeline de criação e execução de uma imagem docker que deverá ser criada e executada em um servidor na nuvem utilizando um Gitlab Runner.

## APLICAÇÃO UTILIZADA

Foi utilizada como base o template de uma aplicação web.

[GYMSTER](https://www.free-css.com/assets/files/free-css-templates/download/page285/gymster.zip)

## PASSOS REALIZADOS

1. Instância de VM na Google Cloud Platform
2. Configurar acesso SSH a VM
3. Instalar o Docker Engine
4. Configurar o GitLab Runner na VM
5. Criar Dockfile da aplicação
6. Implementar Stages e Jobs do arquivo yml de CI-CD do GitLab
